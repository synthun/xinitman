.POSIX:

PREFIX = /usr/local

install:
	mkdir -p $(PREFIX)/bin
	cp -f xinitman $(PREFIX)/bin/xinitman

uninstall:
	rm -rf $(PREFIX)/bin/xinitman
